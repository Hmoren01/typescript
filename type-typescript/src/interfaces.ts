// Función para mostrar una fotografia
export {}; // Separa el contexto de los archivos anteriores al contexto actual-> Quita las dependencias de los archivos externos

enum PhotoOrientation {
    Landscape,
    Portrait,
    Square,
    Panorama
}

interface Picture{
    title: string; 
    date: string;
    orientacion: PhotoOrientation
}

function showPicture(picture:Picture){

    console.log(`[title: ${picture.title}, 
                 date: ${picture.date},
                 orientacion: ${picture.orientacion}]`);
    
}

let myPic = {
    title: 'Test Title',
    date: '2020-03-22',
    orientacion: PhotoOrientation.Landscape
};

showPicture(myPic);
showPicture({
    title: 'Test Title',
    date: '2021-01-13',
    orientacion: PhotoOrientation.Portrait
});

interface PictureConfig{
    title?: string;
    date?: string;
    orientation?: PhotoOrientation
}

function generatePicture(config: PictureConfig){
    const pic = {title: 'Default', date: '2021-01'};
    if(config.title){
        pic.title = config.title;
    }
    if(config.date){
        pic.date= config.date;
    }

    return pic;    
}

let picture = generatePicture({});
console.log('picture', picture);

picture = generatePicture({title: 'Travel pic'});
console.log('picture', picture);

picture = generatePicture({title: 'Travel pic', date: '2021-05'});
console.log('picture', picture);

// Interfaz: Usuario

interface User{
    readonly id: number;  // solo lectura
    username: string;
    isPro: boolean;
}

let user: User;
user = {id: 10, username: 'hernan.alexis.moreno@gmail.com', isPro: true};
console.log('user', user);
user.username = 'Hmoreno';
// user.id= 20; // no se puede cambiar id ya que esta declarada readonly
console.log('user', user);