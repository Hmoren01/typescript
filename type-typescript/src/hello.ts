console.log('Hello Platzi');
console.log('tsc: Compila archivos typescript');
console.log('tsc --init: Crea el archivo tsconfig el cual tiene la configuración de compilación');
console.log('Se configuró "outDir": "./dist", para que al compilar el archivo JS saliera por la carpeta dist');
console.log('una vez compilado, ejecutamos desde dist con el comando "node dist/hello.js"');
console.log('Si quieres al guardar cualquier cambio se compile automaticamente "tsc --watch"');
console.log('Para ejecutar el archivo generado, se debe escribir node y la ruta del archivo .js');
console.log('Ejemplo: node dist/hello.js');

