// Tipo explicito
let isUser: any;
isUser = 1 // Number
isUser = '1'; // string
console.log('idUser', isUser);

// Tipo Inferido
let otherId;
otherId = 1;
otherId = '2';
console.log('otherId', otherId);

let surprise: any = 'Hello TypeScript';
const res =  surprise.substring(6);
console.log('res', res);

// Tipo: Void
// Void es opuesto a any