console.log("Hola TypeScript con no te compro aun Platzi");

// Number
// Explicito
let phone: number;
phone = 1;
phone = 54321;
// phone = 'hola'; // Error

// Inferido
let phoNumber = 34567;
phoNumber = 123456;
// phoNumber = true // Error por tipo de dato

let hex: number = 0xf00d;
let binady: number = 0b1010;
let octal: number = 0o7457;

// Tipo: Boolean
// Tipado Explicito
let isPro: boolean;
isPro = true;
// isPro = 1; // Error

// Inferido
let isUserPro = false;
isUserPro = true;
// isUserPro = 10; // Error

// Tipo Explicito
// String
let username: string = "Hernán";
username = "Nico";
// username = true; // Error tipo de dato string

// Template string
// Uso de back-tick `
let userInfo: string;
userInfo = `
    User info:
    Username: ${username}
    fistName: ${username + " Moreno"}
    phone: ${phone}
    isPro: ${isPro} 
`;
console.log("UserInfo", userInfo);
