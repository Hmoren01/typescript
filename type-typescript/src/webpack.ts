// Creación de webpack

// primero posicionarse en la carpeta del proyecto y ejecutar 

// npm init -y

// esto creará el archivo package.json, el cual contiene una estructura parecida a esta:

// {
//     "name": "photo-app",
//     "version": "1.0.0",
//     "description": "",
//     "main": "index.js",
//     "scripts": {
//       "test": "echo \"Error: no test specified\" && exit 1"
//     },
//     "keywords": [],
//     "author": "",
//     "license": "ISC"
//   }
  

// Luego ejecutar el siguiente comando para instalar TS y webpack

// // -- este comando fue optimizado en sig linea npm install typescript webpack webpack-cli --save-dev
// npm i typescript webpack webpack-cli ts-loader -D

// Luego crear archivo webpack.config.js 

// module.exports = {
//     mode: 'production',
//     entry: './src/main.ts',
//     devtool: 'inline-source-map',
//     resolve: {
//       extensions: ['.ts', '.js'],
//     },
//     module: {
//       rules: [{
//         test: /\.ts$/,
//         use: 'ts-loader',
//         exclude: /node_modules/
//       }]
//     },
//     output: {
//       filename: 'bundle.js'
//     }
//   }


// Luego en package.json en scripts se elimina "Test" y se agrega build con el nombre del archivo 

// "scripts": {
//     "build": "webpack"
// },