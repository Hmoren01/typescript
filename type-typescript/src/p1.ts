var myArray = [1, 2, 1, 3, 3, 1, 2, 1, 5, 1];

let aux: number;
aux = 0;
for (var val of myArray) {
  if (val > aux) {
    aux = val;
  }
}

for (let num = 1; num <= aux; num++) {
  let contador: number;
  contador = 0;
  for (let i = 0; i < myArray.length; i++) {
    if (num == myArray[i]) {
      contador++;
    }
  }

  let asterisco: string = "";
  for (let n = 1; n <= contador; n++) {
    asterisco = asterisco + "*";
  }

  console.log(num, ":", asterisco);
}
