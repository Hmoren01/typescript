// Explicita
let nullVariable: null;
nullVariable = null;
// nullVariable = 1 // Error!

let otherVariable = null;
otherVariable = 'test';

console.log('nullVariable ', nullVariable);
console.log('otherVariable ', otherVariable);

// Undefined
let undefinedVariable: undefined = undefined;
// undefinedVariable = 'test'; // Error

let otherUndefined = undefined;
otherUndefined = 1;

console.log('undefinedVariable ', undefinedVariable);
console.log('otherUndefined ', otherUndefined);

// Null y Undefined: Como Subtipos
// --strictNullChecks -> Con este comando fuerzas al compilador a ser mas estricto
// con la asignación de variables de tipo Null y Undefined
//  tsc src/type-null-undefined.ts --strictNullChecks
// tambien se puede habilitar el comando desde tsconfig.json


// let albumName: string;
// albumName = null;
// albumName = undefined;

// se comentan lineas anteriores ya que en tsconfig.json esta configurado  "strictNullChecks": true, 
// y el compilador las envía como error de compilación  
